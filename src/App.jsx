import { useEffect, useState } from 'react'; //importer les hooks de React
import axios from "axios";
import "./App.css";
import TodoList from "./components/TodoList";

const API_URL = "https://jsonplaceholder.typicode.com/todos";

function App() {
  // déclaration d'un state todos qui va contenir la liste de tâche (setTodos est une fonction pour mettre à jour le state)
  const [todos, setTodos] = useState([]);

  /*Afficher la liste todos lors de la création du composant (avec useEffect)*/

  //useEffect permet d'exécuter un code lorsque le composant est monté dans le DOM
  useEffect(() => {

    //on appelle l'API grâce à la méthode .get de Axios qui permet d'envoyer une requête GET à la route (API_URL)
    axios.get(API_URL)
      //on récupère la réponse (objet response) et la renvoie avec .then() une fois la promise de la requête résolue
      //la réponse récupérée et renvoyée est un objet qui contient les données de l'API dans sa propriété data (cf la console)
      //la réponse est automatiquement convertie en un objet JavaScript par Axios (pas en JSON)
      .then((response) => {
        //affiche le contenu de l'objet response dans la console
        console.log(response);
        //on met à jour le state de todos avec les données contenues dans la propriété data de l'objet response grâce à la fonction setTodos
        //on peut accéder directement aux données de la réponse sans avoir à les convertir puisque déjà en JavaScript
        setTodos(response.data);
      });
  }, []); //le deuxième paramètre du hook useEffect est un tableau vide (useEffect ne doit s'exécuter qu'une seule fois)


  //affiche le composant TodoList avec la liste de tâches todos passée en valeur de props
  //le composant TodoList affiche la liste todos contenue dans le state todos
  return (
    <div className="App">
      <TodoList todos={todos} />
    </div>
  );
}

export default App;
